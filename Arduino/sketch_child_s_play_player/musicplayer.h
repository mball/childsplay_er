#ifndef music_player_h
#define music_player_h

#include "debounced_button.h"
#include "volume_potentiometer.h"
#include <LinkedList.h>
#include <SoftwareSerial.h>

class MusicPlayer
{
  public:

    MusicPlayer(int maxVolume, SoftwareSerial* softSerial); // Construct whithout a explicit volume control
    MusicPlayer(int maxVolume, SoftwareSerial* softSerial,int volumePin); // Construct with a volume control
    ~MusicPlayer(); // Clean up
    void init(); // Initialize the player
    void update(); // Call in main loop, returns
    void getState(); // Prints the state of the connected DFPlayer
    void playFolder(int num); // Start to loop the folder, or play next
    void playPause(); // Toggle Play Pause
    void previous(); //Go to previous song 
    void next();  //Go to next song
    void volumeUp(); // Control volume through buttons, works only when no analog volume control is added
    void volumeDown(); // Control volume through buttons, works only when no analog volume control is added
    void addButton(DebouncedButton* button); //Button to add to the player

  private:

    LinkedList<DebouncedButton*> _buttons;
    int _currentFolder;  //Current Folder
    int _maxVolume;   //Maximum allowed volume
    int _volume; //Volume which is set
    bool _isPlaying; //The player is currently playing or not
    bool _initialized; // the current reading from the input pin
    VolumePotentiometer* _volumeControl;
    SoftwareSerial* _SoftwareSerial; // RX, TX
    void printDetail(uint8_t type, int value); // Print readable version of return value from DFPlayer
};


#endif