// A button which is hardwired to a folder,
// first press leads to first song in folder,
// all further presses jump to the next song in the folder
#ifndef folder_button_h
#define folder_button_h

#include "Arduino.h"
#include "debounced_button.h"
#include "musicplayer.h"

class FolderButton:public DebouncedButton
{
  public:
    FolderButton(int pin, int folder, MusicPlayer* player); // Constructor takes an input pin
    boolean update(); // Overrides function from parent, Called in main loop

  private:
    MusicPlayer* _player;
    int _folder;
};


#endif