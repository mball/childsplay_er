#include <SoftwareSerial.h>
#include "debounced_button.h"
#include "playpause_button.h"
#include "folder_button.h"
#include <DFRobotDFPlayerMini.h>
#include <LinkedList.h>
#include "musicplayer.h"

#define RX_PIN_SOFT 7
#define TX_PIN_SOFT 6
#define ANALOG_VOLUME_PIN A1 // Poti
#define FOLDER_THREE_PIN 2 // Yellow
#define FOLDER_TWO_PIN 3 // White
#define FOLDER_ONE_PIN 4 // Black
#define FOLDER_FOUR_PIN 11 // Blue
#define FOLDER_FIVE_PIN 10 // Green
#define FOLDER_SIX_PIN 9 // Red
#define GENERIC_BUTTON_PIN 12
#define NEXT_BUTTON 18
#define PREVIOUS_BUTTON 19
#define LOW_VOLUME_BUTTON A3

#define HIGH_MAX_VOLUME 28
#define LOW_MAX_VOLUME 18


SoftwareSerial mySoftwareSerial(RX_PIN_SOFT,TX_PIN_SOFT); // RX, TX
MusicPlayer* mPlayer; // Music player which controls the DFPlayer module, and checks for button inputs

void setup() {
  // put your setup code here, to run once:
  mySoftwareSerial.begin(9600); //DFPPlayer
  Serial.begin(115200);
  int maxVolume=HIGH_MAX_VOLUME;
  pinMode(LOW_VOLUME_BUTTON,INPUT_PULLUP);
  if(digitalRead(LOW_VOLUME_BUTTON)){
    maxVolume=LOW_MAX_VOLUME;
  }
  Serial.println(F("Maximum Volume is:"));
  Serial.println(maxVolume);
  mPlayer= new MusicPlayer(maxVolume,&mySoftwareSerial,ANALOG_VOLUME_PIN);
  mPlayer->init();
  mPlayer->addButton(new PlayPauseButton(GENERIC_BUTTON_PIN,mPlayer,PlayPauseButton::ButtonType::playpause));
  mPlayer->addButton(new FolderButton(FOLDER_THREE_PIN,3,mPlayer));
  mPlayer->addButton(new FolderButton(FOLDER_TWO_PIN,2,mPlayer));
  mPlayer->addButton(new FolderButton(FOLDER_ONE_PIN,1,mPlayer));
  mPlayer->addButton(new FolderButton(FOLDER_FOUR_PIN,4,mPlayer));
  mPlayer->addButton(new FolderButton(FOLDER_FIVE_PIN,5,mPlayer));
  mPlayer->addButton(new FolderButton(FOLDER_SIX_PIN,6,mPlayer));
  mPlayer->addButton(new PlayPauseButton(NEXT_BUTTON,mPlayer,PlayPauseButton::ButtonType::next));
  mPlayer->addButton(new PlayPauseButton(PREVIOUS_BUTTON,mPlayer,PlayPauseButton::ButtonType::previous));
}

void loop() {
  mPlayer->update();
}


