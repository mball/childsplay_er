// Class providing simple functions to control the player
// which do not require additional input: Play/Pause, Next, Previous
#ifndef playpause_button_h
#define playpause_button_h

#include "Arduino.h"
#include "debounced_button.h"
#include "musicplayer.h"

class PlayPauseButton:public DebouncedButton
{
  public:
    enum ButtonType { playpause, next, previous, volumeUp, volumeDown};
    PlayPauseButton(int pin, MusicPlayer* player, ButtonType); // Constructor takes an input pin
    boolean update(); // Overrides function from parent, Called in main loop

  private:
    MusicPlayer* _player;
    ButtonType _type;
};


#endif