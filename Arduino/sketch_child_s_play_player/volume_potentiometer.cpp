#include "volume_potentiometer.h"
#include "Arduino.h"

VolumePotentiometer::VolumePotentiometer(int analogPin, int maxVolume, boolean isInverse){
    this->_analogPin=analogPin;
    this->_maxVolume=maxVolume;
    this->_isInverse=isInverse;
}

// Takes the average value of all samples, and convert to a volume. Resets counter and sum.
// If counter is 0 returns 0;
int VolumePotentiometer::getVolume(){
    if(this->_counter==0){
        this->_sum=0;
        return 0;
    }
    int result=(int)(this->_sum/this->_counter);
    this->_sum=0;
    this->_counter=0;
    return getVolumeForPoti(result);
}

//Returns the number of samples
int VolumePotentiometer::update(){
    this->_sum+=analogRead(this->_analogPin);
    return this->_counter++;
}


int VolumePotentiometer::getVolumeForPoti(int analogVal){
    int numerator=analogVal*this->_maxVolume;
    int result=numerator/ANALOG_MAX;
    if(this->_isInverse){
        return (this->_maxVolume-result);
    }else{
        return result;
    }
} 

