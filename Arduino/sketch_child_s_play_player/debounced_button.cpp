#include "Arduino.h"
#include "debounced_button.h"

//Standard constructor needed for LinkedList.pop().
DebouncedButton::DebouncedButton(){
  this->_pin=-1;
  this->_lastButtonState=LOW;
  this->_lastDebounceTime=0L;
}

DebouncedButton::DebouncedButton(int pin){
    pinMode(pin, INPUT_PULLUP);
    this->_pin=pin;
    this->_lastButtonState=LOW;
    this->_lastDebounceTime=0L;
    //Serial.println(F("Const"));
}

int DebouncedButton::getPin(){
  return _pin;
}

boolean DebouncedButton::update(){
  // read the state of the switch into a local variable:
  int readState = digitalRead(_pin);
  // check to see if you just pressed the button
  // (i.e. the input went from LOW to HIGH), and you've waited long enough
  // since the last press to ignore any noise:
  // If the switch changed, due to noise or pressing:
  if (readState != this->_lastButtonState) {
    // reset the debouncing timer
    this->_lastDebounceTime = millis();
  }
  if ((millis() - this->_lastDebounceTime) > DEBOUNCE_DELAY) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:
    // if the button state has changed:
    this->_lastButtonState = readState;
    if (readState != this->_buttonState) {
      this->_buttonState = readState;
      if (this->_buttonState == LOW) {  
        //this->_callback();
        return true; // Only here we have a valid edge to low
      }
    }
  }
  this->_lastButtonState = readState;
  return false;
}

DebouncedButton& DebouncedButton::operator=(const DebouncedButton& buttonToCopy){
  this->_lastButtonState=buttonToCopy._lastButtonState;
  this->_lastDebounceTime=buttonToCopy._lastDebounceTime;
  this->_pin=buttonToCopy._pin;
  this->_buttonState=buttonToCopy._buttonState;
  return *this;
} 

