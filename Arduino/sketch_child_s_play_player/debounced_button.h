//Class to debounce a button in software.
//checks for changed state to low
#ifndef debounced_button_h
#define debounced_button_h

#include "Arduino.h"

class DebouncedButton
{
  public:

    DebouncedButton(); // DO NOT USE, always initiate with input pin 
    DebouncedButton(int pin); // Constructor take an input pin
    virtual boolean update(); // Call in main loop, returns true if changed state to low
    DebouncedButton& operator=(const DebouncedButton&); // Copy operator for linked list
    int getPin(); // Get the pin of the button 

  private:

    int _pin; //Input pin 
    int _buttonState;             // the current reading from the input pin
    int _lastButtonState;   // the previous reading from the input pin
    unsigned long _lastDebounceTime;  // the last time the output pin was toggled

    //Debounce Time as a constant
    const unsigned long DEBOUNCE_DELAY = 50UL;    // the debounce time; increase if the output flickers
};



#endif