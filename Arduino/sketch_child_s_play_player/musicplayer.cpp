#include <Arduino.h>
#include <SoftwareSerial.h>
#include "musicplayer.h"
#include "debounced_button.h"
#include <DFRobotDFPlayerMini.h>
#include <LinkedList.h>

DFRobotDFPlayerMini dFPlayerInstance;

MusicPlayer::MusicPlayer(int maxVolume, SoftwareSerial* softSerial){
    this->_currentFolder=-1; //Initialize with invalid value
    this->_maxVolume= maxVolume;
    this->_volume= (maxVolume/3);
    this->_isPlaying= false;
    this->_initialized= false; // the current reading from the input pin
    this->_SoftwareSerial= softSerial; // RX, TX
    this->_buttons=LinkedList<DebouncedButton*>();
    this->_volumeControl=NULL;
}

MusicPlayer::MusicPlayer(int maxVolume, SoftwareSerial* softSerial, int volumePin){
    this->_currentFolder=-1; //Initialize with invalid value
    this->_maxVolume= maxVolume;
    this->_volume= 0;
    this->_isPlaying= false;
    this->_initialized= false; // the current reading from the input pin
    this->_SoftwareSerial= softSerial; // RX, TX
    this->_buttons=LinkedList<DebouncedButton*>();
    this->_volumeControl= new VolumePotentiometer(volumePin, maxVolume, false); // For now it is inverse
}


void MusicPlayer::init(){
  if (!dFPlayerInstance.begin(*(this->_SoftwareSerial))) {  //Use softwareSerial to communicate with mp3.
    Serial.println(F("Unable to begin:"));
    Serial.println(F("1.Please recheck the connection!"));
    Serial.println(F("2.Please insert the SD card!"));
    while(true);
  }
  Serial.println(F("DFPlayer Mini online."));
  if(_volumeControl!=NULL){
     this->_volumeControl->update();
     this->_volume=this->_volumeControl->getVolume();
  }
  dFPlayerInstance.volume(this->_volume);  //Set volume value. From 0 to 30 //25 is nearly maximum
  //dFPlayerInstance.play(4);
  this->_initialized=true;
  getState();
  //dFPlayerInstance.playFolder(1,0);
  //dFPlayerInstance.loopFolder(1);
}

void MusicPlayer::addButton(DebouncedButton* button){
  this->_buttons.add(button);
  int pin=this->_buttons.get(this->_buttons.size()-1)->getPin();
  //Serial.print(pin);
}


void MusicPlayer::update(){
  //Check Buttons
  for(int i=0;i<this->_buttons.size();i++){
    if(this->_buttons.get(i)->update()){
        //Serial.println("Button Event");
    }
  }
  if(this->_volumeControl!=NULL &&
    this->_volumeControl->update()>=100){
     int newVolume=this->_volumeControl->getVolume();
     if(newVolume!=this->_volume){
         dFPlayerInstance.volume(newVolume);
         this->_volume=newVolume; 
         //Serial.println("Changed volume to");
         //Serial.println(newVolume);
      }
  }

  //Print received messages  
  if (dFPlayerInstance.available()) {
    printDetail(dFPlayerInstance.readType(), dFPlayerInstance.read()); //Print the detail message from DFPlayer to handle different errors and states.
  }
}

void MusicPlayer::getState(){
  //TODO: What did i want?
}


//Public functions
void MusicPlayer::playFolder(int num){
    if(this->_currentFolder==num){ //Maybe check if valid folder
        dFPlayerInstance.next();
    }else{
        dFPlayerInstance.loopFolder(num);
    }
    this->_isPlaying=true;
    this->_currentFolder=num;
}

void MusicPlayer::playPause(){
    if(this->_currentFolder<0){
      //No folder selected choose the first folder
      this->playFolder(1);
    }else{
      if(this->_isPlaying){
          dFPlayerInstance.pause();
      }else{
          dFPlayerInstance.start();
      }
    }
    this->_isPlaying=!(this->_isPlaying);
}

void MusicPlayer::next(){
   if(this->_currentFolder<0){
      //No folder selected choose the first folder
      this->playFolder(1);
    }else{
      this->playFolder(this->_currentFolder);
    }
}

void MusicPlayer::previous(){
   if(this->_currentFolder<0){
      //No folder selected choose the first folder and start to loop
      this->playFolder(1);
    }else{
      dFPlayerInstance.previous();
      this->playFolder(this->_currentFolder);
    }
}

void MusicPlayer::volumeDown(){
    int newVolume=this->_volume-1;
    this->_volume=newVolume<0?0:newVolume;
    dFPlayerInstance.volume(this->_volume);
}

void MusicPlayer::volumeUp(){
    int newVolume=this->_volume+1;
    this->_volume=newVolume>this->_maxVolume?this->_maxVolume:newVolume;
    dFPlayerInstance.volume(this->_volume);
}




void MusicPlayer::printDetail(uint8_t type, int value){
  switch (type) {
    case TimeOut:
      Serial.println(F("Time Out!"));
      break;
    case WrongStack:
      Serial.println(F("Stack Wrong!"));
      break;
    case DFPlayerCardInserted:
      Serial.println(F("Card Inserted!"));
      break;
    case DFPlayerCardRemoved:
      Serial.println(F("Card Removed!"));
      break;
    case DFPlayerCardOnline:
      Serial.println(F("Card Online!"));
      break;
    case DFPlayerPlayFinished:
      Serial.print(F("Number:"));
      Serial.print(value);
      Serial.println(F(" Play Finished!"));
      break;
    case DFPlayerError:
      Serial.print(F("DFPlayerError:"));
      switch (value) {
        case Busy:
          Serial.println(F("Card not found"));
          break;
        case Sleeping:
          Serial.println(F("Sleeping"));
          break;
        case SerialWrongStack:
          Serial.println(F("Get Wrong Stack"));
          break;
        case CheckSumNotMatch:
          Serial.println(F("Check Sum Not Match"));
          break;
        case FileIndexOut:
          Serial.println(F("File Index Out of Bound"));
          break;
        case FileMismatch:
          Serial.println(F("Cannot Find File"));
          break;
        case Advertise:
          Serial.println(F("In Advertise"));
          break;
        default:
          break;
      }
      break;
    default:
      break;
    }
}

MusicPlayer::~MusicPlayer(){
    if(_volumeControl!=NULL){
      delete _volumeControl;
      _volumeControl=NULL;
    }
}

