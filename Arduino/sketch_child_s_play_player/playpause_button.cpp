#include "playpause_button.h"

PlayPauseButton::PlayPauseButton(int pin, MusicPlayer* player, ButtonType button_type):DebouncedButton(pin){
    this->_player=player;
    this->_type=button_type;
}

boolean PlayPauseButton::update(){
    if(DebouncedButton::update()==true){
        switch(this->_type){
            case playpause:
                this->_player->playPause();
                break;
            case next:
                this->_player->next();
                break;
            case previous:
                this->_player->previous();
                break;
            case volumeUp:
                this->_player->volumeUp();
                break;
            case volumeDown:
                this->_player->volumeDown();
                break;
            default:
                Serial.println("ButtonType not implemented");
        }
       
    }
    return DebouncedButton::update();
}
