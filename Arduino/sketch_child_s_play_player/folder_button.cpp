#include "folder_button.h"

FolderButton::FolderButton(int pin, int folder, MusicPlayer* player):DebouncedButton(pin){
    this->_player=player;
    this->_folder=folder;
}

boolean FolderButton::update(){
    boolean state=DebouncedButton::update();
    if(state==true){
        this->_player->playFolder(this->_folder);
    }
    return state;
}
