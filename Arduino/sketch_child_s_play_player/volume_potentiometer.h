#ifndef volume_potentiometer_h
#define volume_potentiometer_h

#include "Arduino.h"

class VolumePotentiometer{

        public:

        VolumePotentiometer(int analogPin, int maxVolume, boolean isInverse);
        int getVolume(); // Takes the average value of all samples, and convert to a volume. Resets counter and sum.
        int update(); //Returns the number of samples

        private:
        
        int _analogPin; // The pin for an analog read should be performed
        int _maxVolume; // Maximum allowed value
        int _counter; // Number of read samples
        long _sum; // Sum of read values
        boolean _isInverse; // Whether the analog pin is inverse to the reading
        int getVolumeForPoti(int analogVal);
        const int ANALOG_MAX= 1023; // Maximum readable value from analogRead in Arduino
};

#endif