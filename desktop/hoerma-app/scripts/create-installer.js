const electronInstaller = require("electron-winstaller");
const path = require("path");

async function run() {
  try {
    console.log("Create Installer");
    await electronInstaller.createWindowsInstaller({
      appDirectory: path.join(process.cwd(), "/release/hoerma-win32-ia32"),
      outputDirectory: path.join(process.cwd(), "/installer/win32"),
      authors: "MBalles",
      exe: "hoerma.exe",
      name: "HoermaApp",
      description: "Manage SD-Card for Hoerma"
    });
    console.log("Created Installer.");
  } catch (e) {
    console.log(`Creating installer failed: ${e.message}`);
  }
}

run();
