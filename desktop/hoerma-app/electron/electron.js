const { app, BrowserWindow, shell, ipcMain } = require("electron");
const { persistSongList, generateMenu, initializeFromFile } = require("./main-process/index.js");
const path = require("path");
const isDev = require("electron-is-dev");
const handleSquirrelEvent = require("./squirrel");

if (require("electron-squirrel-startup")) return;

if (handleSquirrelEvent()) {
  // squirrel event handled and app will exit in 1000ms, so don't do anything else
  return;
}

let mainWindow;

const addDevTools = () => {
  const { default: installExtension, REACT_DEVELOPER_TOOLS, REDUX_DEVTOOLS } = require("electron-devtools-installer");

  installExtension(REACT_DEVELOPER_TOOLS)
    .then(name => {
      console.log(`Added Extension: ${name}`);
    })
    .catch(err => {
      console.log("An error occurred: ", err);
    });

  installExtension(REDUX_DEVTOOLS)
    .then(name => {
      console.log(`Added Extension: ${name}`);
    })
    .catch(err => {
      console.log("An error occurred: ", err);
    });
};

const createWindow = async () => {
  await initializeFromFile();
  mainWindow = new BrowserWindow({
    backgroundColor: "#F7F7F7",
    minWidth: 400,
    show: false,
    titleBarStyle: "hidden",
    webPreferences: {
      nodeIntegration: true,
      preload: __dirname + "/preload.js"
    },
    height: 860,
    width: 1280,
    icon: "../icon.ico"
  });

  mainWindow.loadURL(isDev ? "http://localhost:3000" : `file://${path.join(__dirname, "../build/index.html")}`);

  if (isDev || process.env.SHOW_DEV_TOOLS) {
    addDevTools();
  }

  mainWindow.once("ready-to-show", () => {
    mainWindow.show();

    ipcMain.on("open-external-window", (event, arg) => {
      shell.openExternal(arg);
    });
  });
};

ipcMain.on("load-page", (event, arg) => {
  console.log("load-page");
  mainWindow.loadURL(arg);
});

app.on("ready", async () => {
  console.log("ready:create window + generateMenu()");
  await createWindow();
  generateMenu(isDev || process.env.SHOW_DEV_TOOLS);
});

app.on("close", async () => {
  //persist state
  console.log("Close called, persist state");
  await persistSongList();
});

app.on("window-all-closed", async () => {
  console.log("All windows closed called");
  await persistSongList();
  app.quit();
});

app.on("activate", async () => {
  if (mainWindow === null) {
    console.log("activate:create window");
    await createWindow();
  }
});
