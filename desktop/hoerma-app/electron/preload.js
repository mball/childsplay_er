//Make the ipcRenderer accessible for the Webapp.

const ipcRenderer = require("electron").ipcRenderer;

const requestSongList = folderName => {
  ipcRenderer.send("RequestSongList", { name: folderName });
};

const loadFolderFromFile = folderName => {
  ipcRenderer.send("LoadFolder", { name: folderName });
};

const saveFolderToFile = folderName => {
  ipcRenderer.send("SaveFolder", { name: folderName });
};

const updateFolder = (folderName, files, clearListRequest) => {
  ipcRenderer.send("UpdateFolder", {
    folder: {
      songs: files,
      name: folderName
    },
    clearListRequest
  });
};

const writeSDCard = () => {
  ipcRenderer.send("WriteRequest");
};

const registerNotifyUserListener = listener => {
  ipcRenderer.on("notifyUser", listener);
};

const removeNotifyUserListener = listener => {
  ipcRenderer.removeListener("notifyUser", listener);
};

const registerLoadedSongListListener = listener => {
  ipcRenderer.on("LoadedSongList", listener);
};

const removeLoadedSongListListener = listener => {
  ipcRenderer.remove("LoadedSongList", listener);
};

window.childsPlayer = {
  requestSongList,
  updateFolder,
  loadFolderFromFile,
  saveFolderToFile,
  writeSDCard,
  registerNotifyUserListener,
  registerLoadedSongListListener,
  removeNotifyUserListener,
  removeLoadedSongListListener
};

window.buttons = require("./lib/cpconfig.json").buttons;
