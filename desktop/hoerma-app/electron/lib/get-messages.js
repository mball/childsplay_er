const template = require("lodash.template");

const messages = {
  fileMenu: "Datei",
  fileMenuSDCard: "SD-Karte auswählen",
  dialogSDCardTitle: "Ordner für SD-Karte auswählen …",
  dialogSaveListTitle: "Liste speichern nach …",
  dialogLoadListTitle: "Liste laden von …",
  clearedList: "Liste gelöscht",
  wroteSDCard: "Hoerma erfolgreich beschrieben.",
  writeSDCardErrorDelete: "Daten auf der SD-Karte konnten nicht gelöscht werden."
};

/* eslint-disable no-template-curly-in-string */
const messagesWithTemplate = {
  folderSaved: template('Liste gespeichert unter "${file}"'),
  folderSaveFailed: template('Speichern der Liste in "${file}" fehlgeschlagen'),
  folderLoadFailed: template('Liste "${file}" konnte nicht geladen werden'),
  writeSDErrorMissingIndexFile: template(
    ' "hoerma.json" konnte im Pfad "${file}" nicht gefunden werden. Schreiben abgebrochen.'
  ),
  writeSDErrorSongCopyFailed: template('Song "${file}" kann nicht kopiert werden. Schreiben abgbrochen.')
};
/* eslint-enable no-template-curly-in-string */

const getMessageMethodByKey = key => (messagesWithTemplate[key] ? messagesWithTemplate[key] : () => messages[key]);

const getMessage = key => getMessageMethodByKey(key);

module.exports = getMessage;
