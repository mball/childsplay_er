const Store = require("electron-store");
const path = require("path");
const { app } = require("electron");

let settingsCache;

const sdCardPathKey = "sdCardPath";
const listFileKey = "pathToListFile";
const folderFilesDirectoryKey = "folderFilesDirectory";

const defaultSettings = {
  [sdCardPathKey]: "E:\\",
  [listFileKey]: path.join(app.getPath("userData"), "lists/songlist.json"),
  [folderFilesDirectoryKey]: path.join(app.getPath("documents"), "/Hoerma/beispiel-liste.json")
};

const userSettings = new Store({ defaults: defaultSettings });

settingsCache = userSettings.store; // initialize the cache

console.log(`Current settings: ${JSON.stringify(settingsCache)}`);

const setSettings = key => value => {
  console.log(`Set ${key} to ${value}`);
  try {
    userSettings.set(key, value);
  } catch (error) {
    console.error("Failed to set setting: ", error.message);
    return;
  }
  settingsCache[key] = value;
};

const getSettings = key => () => {
  if (!settingsCache[key]) {
    settingsCache[key] = userSettings.get(key);
  }
  console.log(`Get setting ${key}: ${settingsCache[key]}`);
  return settingsCache[key];
};

const setPathToListFile = setSettings(listFileKey);
const setPathToSDCard = setSettings(sdCardPathKey);
const setPathToFolderFiles = setSettings(folderFilesDirectoryKey);

const getPathToListFile = getSettings(listFileKey);
const getPathToSDCard = getSettings(sdCardPathKey);
const getPathToFolderFiles = getSettings(folderFilesDirectoryKey);

module.exports = {
  getPathToListFile,
  getPathToSDCard,
  setPathToListFile,
  setPathToSDCard,
  setPathToFolderFiles,
  getPathToFolderFiles
};
