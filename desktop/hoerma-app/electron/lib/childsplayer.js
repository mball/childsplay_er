const { promises: fs, constants: FILE_CONSTANTS } = require("fs");
const path = require("path");
const config = require("./cpconfig.json");
const { getPathToListFile, setPathToListFile } = require("./user-settings");
const { promisify } = require("util");
const asyncRimraf = promisify(require("rimraf"));

class WriteSDError extends Error {
  constructor(message, messageKey, context) {
    super(message);
    this.userInfo = {
      messageKey,
      context
    };
  }
}

const getButtons = function() {
  return config.buttons;
};

const loadFoldersFromFile = async () => {
  let folders;
  const fileWithFolders = getPathToListFile();
  try {
    folders = JSON.parse(await fs.readFile(fileWithFolders));
  } catch (error) {
    console.log(`Failed to open file ${fileWithFolders}`);
  }
  if (checkForValidity(folders)) {
    return folders;
  } else {
    console.log("Empty file or invalid structure returned from file, initializes with valid structure");
    return getInitialFoldersStructure();
  }
};

const loadSongsFromFile = async fileWithSongs => {
  let songs;
  try {
    songs = JSON.parse(await fs.readFile(fileWithSongs));
  } catch (error) {
    console.log(`Failed to open file ${fileWithSongs}`);
    return null;
  }
  if (!validateSongs(songs)) {
    console.log(`Invalid songs`);
    return null;
  }
  return songs;
};

const getInitialFoldersStructure = () => {
  var folders = config.buttons.map(button => {
    return { name: button.name, songs: [] };
  });
  return folders;
};

const setPathToList = async pathToList => {
  try {
    await fs.access(pathToList, FILE_CONSTANTS.W_OK);
  } catch (error) {
    if (error.code !== "ENOENT") {
      console.error(`Can't Access the provided file: ${pathToList}, reason: ${error.message}`);
    } else {
      console.error(`File not found: ${pathToList}`);
    }
    return false;
  }
  setPathToListFile(pathToList);
  return true;
};

const createDirectoryIfNecessary = async directory => {
  try {
    await fs.access(path.dirname(directory), FILE_CONSTANTS.F_OK);
  } catch (error) {
    console.log(`File ${directory} doesn't seem to exist, trying to create it`);
    try {
      await fs.mkdir(path.dirname(directory), { recursive: true });
    } catch (error) {
      console.log(`Failed to create directory for: ${directory}`);
      return false;
    }
  }
  return true;
};

const validateSongs = songs => {
  const validSongs = songs.filter(song => song.name && song.path);
  return songs.length === validSongs.length;
};

const saveSongsToFile = async (songs, filePath) => {
  if (!songs && Array.isArray(songs) && validateSongs(songs)) {
    console.error("Songs which should be saved are not valid");
    return false;
  }
  if (!(await createDirectoryIfNecessary(filePath))) {
    console.log(`Failed to create directory: ${filePath}`);
    return false;
  }
  try {
    await fs.writeFile(filePath, JSON.stringify(songs));
  } catch (error) {
    console.log(`Failed to write to file: ${songs}`);
    return false;
  }
  return true;
};

const writeFoldersToFile = async folders => {
  //Compare with required form by config.
  if (!checkForValidity(folders)) {
    console.log(`Invalid folders provided: ${JSON.stringify(folders)}`);
    return false;
  }
  const pathToList = getPathToListFile();
  //Valid form try to write to file.
  if (!(await createDirectoryIfNecessary(pathToList))) {
    console.log(`Failed to create directory: ${pathToList}`);
    return false;
  }
  try {
    await fs.writeFile(pathToList, JSON.stringify(folders));
  } catch (error) {
    console.log(`Failed to write to file: ${pathToList}`);
    return false;
  }
  return true;
};

//Copy the data according to the config list
const writeSDCard = async (pathSDCard, folders) => {
  if (!folders) {
    console.log("No folders requested, load default from file");
    folders = await loadFoldersFromFile();
  }
  console.log(`Directory to write to: ${pathSDCard}`);

  // Check if directory has `hoerma.json`
  const indexFilePath = path.join(pathSDCard, "hoerma.json");
  try {
    await fs.access(path.join(pathSDCard, "hoerma.json"), FILE_CONSTANTS.F_OK);
  } catch (error) {
    throw new WriteSDError(
      `File ${indexFilePath} doesn't exist, can't write to folder`,
      "writeSDErrorMissingIndexFile",
      { file: indexFilePath }
    );
  }

  //First of all clean up SDCard.
  console.log("Deleting existing files");
  try {
    await asyncRimraf(path.join(pathSDCard, "/*"));
  } catch (error) {
    console.error(`Failed to delete: ${error.message}`);
    throw new WriteSDError("Data on SD-Card could not be deleted", "writeSDCardErrorDelete");
  }
  console.log("Deleted Folders");
  //Sort buttons ascending

  await fs.copyFile(getPathToListFile(), indexFilePath);

  config.buttons.sort((firstButton, secondButton) => {
    return firstButton.folder - secondButton.folder;
  });

  await Promise.all(
    config.buttons.map(async button => {
      const { songs } = folders.find(folder => {
        return folder.name === button.name;
      });
      if (songs.length > 0) {
        //At least one song create folder and add files
        //TODO: Careful, verify 'folder' < 99
        const folderName = ("0" + button.folder).slice(-2);
        await fs.mkdir(path.join(pathSDCard, folderName));
        let index = 0;
        const songsWithIndex = songs.map(song => {
          index++;
          return { ...song, index: index - 1 };
        });
        await Promise.all(
          songsWithIndex.map(async ({ name, path: pathToFile, index }) => {
            const songName = ("00" + index).slice(-3) + " " + name;
            try {
              await fs.copyFile(pathToFile, path.join(pathSDCard, folderName, songName));
            } catch (error) {
              console.error(error.message, error.stack);
              throw new WriteSDError(`Failed to copy song ${name}}`, "writeSDErrorSongCopyFailed", { file: name });
            }
          })
        );
        console.log(`${folderName} written`);
      }
    })
  );
  return true;
};

//Check if list object is a valid array which can be written to the file
const checkForValidity = function(folders) {
  if (folders == null || folders === undefined || config.buttons.length !== folders.length) {
    return false;
  }
  config.buttons.forEach(button => {
    if (
      !folders.find(folder => {
        return folder.name === button.name;
      })
    ) {
      console.log("Folder name" + button.name + "not found");
      return false;
    }
  });
  return true;
};

module.exports = {
  getButtons,
  setPathToList,
  writeFoldersToFile,
  loadFoldersFromFile,
  saveSongsToFile,
  loadSongsFromFile,
  writeSDCard
};
