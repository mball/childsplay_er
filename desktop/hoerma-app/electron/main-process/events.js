const { ipcMain } = require("electron");
const { openFolderFileDialog, saveFolderFileDialog } = require("./dialogs");
const {
  requestSongList,
  updateFolder,
  writeMP3stoSDCard,
  saveFolderToFile,
  updateFolderFromFile
} = require("./main.js");
const getMessage = require("../lib/get-messages");
const notifyUser = require("./notification");

const clearedListMessage = getMessage("clearedList")();
const wroteSDCardMessage = getMessage("wroteSDCard")();

ipcMain.on("WriteRequest", async (event, arg) => {
  console.log("Write requested");
  //Write current state to list
  let success;
  let message;
  try {
    success = await writeMP3stoSDCard();
    message = wroteSDCardMessage;
  } catch (error) {
    if (error.userInfo) {
      const { messageKey, context } = error.userInfo;
      message = getMessage(messageKey)(context);
    } else {
      message = error.message;
    }
  }
  notifyUser(event.sender, {
    message,
    level: success ? "success" : "error"
  });
});

ipcMain.on("RequestSongList", (event, arg) => {
  const result = requestSongList(arg);
  console.log("Load Result:" + JSON.stringify(result));
  event.sender.send("LoadedSongList", result ? result.songs : []);
});

ipcMain.on("UpdateFolder", (event, arg) => {
  const { folder, clearListRequest = false } = arg;
  updateFolder(folder);
  if (clearListRequest) {
    const result = requestSongList(folder);
    event.sender.send("LoadedSongList", result.songs);
    notifyUser(event.sender, { message: clearedListMessage, level: "success" });
  }
});

ipcMain.on("LoadFolder", async (event, { name }) => {
  console.log("Load folder:" + name);
  const file = await openFolderFileDialog();
  if (!file) {
    return;
  }
  if (await updateFolderFromFile(name, file)) {
    const result = requestSongList({ name });
    event.sender.send("LoadedSongList", result.songs);
  } else {
    notifyUser(event.sender, { message: getMessage("folderLoadFailed")({ file }), level: "error" });
  }
});

ipcMain.on("SaveFolder", async (event, { name }) => {
  console.log("Save folder:" + name);
  const file = await saveFolderFileDialog();
  if (!file) {
    console.log("No file provided. Returning");
    return;
  }
  if (await saveFolderToFile(requestSongList({ name }), file)) {
    notifyUser(event.sender, { message: getMessage("folderSaved")({ file }), level: "info" });
  } else {
    notifyUser(event.sender, { message: getMessage("folderSaveFailed")({ file }), level: "error" });
  }
});
