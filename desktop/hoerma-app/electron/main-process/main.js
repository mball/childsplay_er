const {
  writeSDCard,
  loadFoldersFromFile,
  setPathToList,
  writeFoldersToFile,
  saveSongsToFile,
  loadSongsFromFile
} = require("../lib/childsplayer");
const { getPathToSDCard, getPathToListFile } = require("../lib/user-settings");

let songList = []; // In memory cache for songlist

function requestSongList({ name }) {
  return songList.find(folder => {
    return folder.name === name;
  });
}

function updateFolder(folder) {
  console.log("Update folder:" + JSON.stringify(folder));
  let index = songList.findIndex(oldList => {
    return oldList.name === folder.name;
  });
  if (index >= 0) {
    songList[index] = folder;
  } else {
    console.error("Invalid input, dismissed without saving");
  }
}

async function updateFolderFromFile(folderName, filePath) {
  const songs = await loadSongsFromFile(filePath);
  if (songs) {
    updateFolder({ name: folderName, songs });
    return true;
  }
  return false;
}

async function saveFolderToFile(folder, filePath) {
  const { songs } = folder;
  console.log(`Saving ${songs.length} songs for ${folder.name} to ${filePath}`);
  if (await saveSongsToFile(songs, filePath)) {
    return true;
  }
  console.log(`Saving songs failed`);
  return false;
}

async function writeMP3stoSDCard() {
  await persistSongList();
  return writeSDCard(getPathToSDCard());
}

async function persistSongList() {
  await writeFoldersToFile(songList);
}

async function initializeFromFile() {
  console.log("List File: " + getPathToListFile());
  await setPathToList(getPathToListFile()); //load write file
  console.log(`Path to SD-Card: ${getPathToSDCard()}`);
  songList = await loadFoldersFromFile();
}

module.exports = {
  requestSongList,
  updateFolder,
  writeMP3stoSDCard,
  initializeFromFile,
  persistSongList,
  saveFolderToFile,
  updateFolderFromFile
};
