const {
  setPathToSDCard,
  getPathToSDCard,
  getPathToFolderFiles,
  setPathToFolderFiles
} = require("../lib/user-settings.js");
const { dialog } = require("electron");
const getMessage = require("../lib/get-messages");

const dialogSDCardTitle = getMessage("dialogSDCardTitle")();
const dialogSaveListTitle = getMessage("dialogSaveListTitle")();
const dialogLoadListTitle = getMessage("dialogLoadListTitle")();

const chooseDirectoryDialog = async () => {
  try {
    const options = {
      title: dialogSDCardTitle,
      defaultPath: getPathToSDCard(),
      properties: ["openDirectory"]
    };

    const directories = await dialog.showOpenDialog(options);
    console.log(`Result from sd-card setting ${directories}`);
    if (directories && directories.length > 0) {
      setPathToSDCard(directories[0]);
    }
  } catch (error) {
    console.error(error.message, error.stack);
  }
};

const openFolderFileDialog = async () => {
  const options = {
    title: dialogLoadListTitle,
    defaultPath: getPathToFolderFiles(),
    properties: ["openFile"],
    filters: [{ name: "Hoerma folder", extensions: ["json"] }]
  };

  const filePaths = await dialog.showOpenDialog(options);
  if (filePaths.length < 1) {
    return null;
  }
  setPathToFolderFiles(filePaths[0]);
  return filePaths[0];
};

const saveFolderFileDialog = async () => {
  const options = {
    title: dialogSaveListTitle,
    defaultPath: getPathToFolderFiles(),
    filters: [{ name: "Hoerma folder", extensions: ["json"] }]
  };
  const file = await dialog.showSaveDialog(options);
  if (file) {
    setPathToFolderFiles(file);
  }
  return file;
};

module.exports = {
  chooseDirectoryDialog,
  openFolderFileDialog,
  saveFolderFileDialog
};
