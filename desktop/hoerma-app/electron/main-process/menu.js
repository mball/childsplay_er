const { Menu } = require("electron");
const { chooseDirectoryDialog } = require("./dialogs.js");
const getMessage = require("../lib/get-messages");

const fileMenu = getMessage("fileMenu")();
const fileMenuSDCard = getMessage("fileMenuSDCard")();

const generateMenu = isDev => {
  let template;
  const sdCardFolderSubmenu = { label: fileMenuSDCard, click: async () => chooseDirectoryDialog() };
  if (isDev) {
    template = [
      {
        label: fileMenu,
        submenu: [{ role: "about" }, sdCardFolderSubmenu, { role: "quit" }]
      },
      {
        label: "Edit",
        submenu: [
          { role: "undo" },
          { role: "redo" },
          { type: "separator" },
          { role: "cut" },
          { role: "copy" },
          { role: "paste" },
          { role: "pasteandmatchstyle" },
          { role: "delete" },
          { role: "selectall" }
        ]
      },
      {
        label: "View",
        submenu: [
          { role: "reload" },
          { role: "forcereload" },
          { role: "toggledevtools" },
          { type: "separator" },
          { role: "resetzoom" },
          { role: "zoomin" },
          { role: "zoomout" },
          { type: "separator" },
          { role: "togglefullscreen" }
        ]
      },
      {
        role: "window",
        submenu: [{ role: "minimize" }, { role: "close" }]
      }
    ];
  } else {
    template = [
      {
        label: fileMenu,
        submenu: [sdCardFolderSubmenu]
      }
    ];
  }
  Menu.setApplicationMenu(Menu.buildFromTemplate(template));
};

module.exports = {
  generateMenu
};
