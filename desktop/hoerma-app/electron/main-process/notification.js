// Possible levels are: success, error, warning or info

const notifyUser = (reciever, { message, level }) => {
  reciever.send("notifyUser", { message, level });
};

module.exports = notifyUser;
