require("./events.js"); //initializes the listeners
const { persistSongList, initializeFromFile } = require("./main");
const { generateMenu } = require("./menu");

module.exports = {
  persistSongList,
  generateMenu,
  initializeFromFile
};
