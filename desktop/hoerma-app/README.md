# Electron-App

## Purpose

With this App you one can add new mp3-files to an SD-Card in a format needed by hoerma to function.

## General

This app is an electron app using react in the frontend.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

The App in action:
https://drive.google.com/file/d/11OTjrjPgJD8hErbb8D_f14KTetsyCbup/view?usp=sharing
