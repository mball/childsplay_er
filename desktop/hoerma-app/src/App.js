import React, { Component } from "react";
import Hoerma from "./components/hoerma";
import MusicList from "./components/music-list";
import "./css/App.css";
import { SnackbarProvider } from "notistack";
import Notifications from "./components/notifications";
import getMessage from "./messages";

const {
  childsPlayer: { writeSDCard },
  buttons
} = window;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedFolder: undefined
    };
    this.onWrite = this.onWrite.bind(this);
    this.onFolderButtonClicked = this.onFolderButtonClicked.bind(this);
  }

  onFolderButtonClicked = event => {
    console.log("The following button was clicked:");
    console.log("ID:" + event.target.id);
    const result = buttons.find(element => element.name === event.target.id);
    this.setState({
      selectedFolder: result
    });
  };

  onWrite = () => {
    writeSDCard();
  };

  render() {
    return (
      <SnackbarProvider maxSnack={3}>
        <div className="App">
          <Hoerma
            folderButtons={buttons}
            selectedFolder={this.state.selectedFolder}
            onFolderButtonClicked={this.onFolderButtonClicked}
          />
          <button onClick={this.onWrite}>{getMessage("writeSDCardButton")()}</button>

          {this.state.selectedFolder === undefined ? (
            <p>{getMessage("clickAButtonText")()}</p>
          ) : (
            <MusicList folderName={this.state.selectedFolder.name} folderColor={this.state.selectedFolder.color} />
          )}
          <Notifications></Notifications>
        </div>
      </SnackbarProvider>
    );
  }
}

export default App;
