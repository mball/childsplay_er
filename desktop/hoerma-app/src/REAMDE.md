# Webpage with React

Bootstrapped from the `create-react-app`.
Files in here will be bundled by `webpack` into the `build` folder.
`src/index.js` is the entry point as designed by `create-react-app`and
should not be moved or renamed.
