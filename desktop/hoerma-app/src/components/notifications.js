import React, { useEffect } from "react";
import { useSnackbar } from "notistack";
import getMessage from "../messages";
const { registerNotifyUserListener, removeNotifyUserListener } = window.childsPlayer;

const missingMessage = getMessage("missingMessage")();

const Notifications = () => {
  const { enqueueSnackbar } = useSnackbar();

  const notificationListener = (event, { message = missingMessage, level }) => {
    const options = { variant: level ? level : "default" };
    enqueueSnackbar(message, options);
  };

  useEffect(() => {
    registerNotifyUserListener(notificationListener);
    return function cleanup() {
      removeNotifyUserListener(notificationListener);
    };
  });

  return <div></div>;
};

export default Notifications;
