import React, { Component } from "react";
import hoerma_sketch from "../static-assests/hoerma_sketch.png";
import "../css/App.css";

export default class Hoerma extends Component {
  getClassName(forButton) {
    if (this.props.selectedFolder === undefined || this.props.selectedFolder === {}) {
      return "button-border";
    }
    if (this.props.selectedFolder.name === forButton.name) {
      return "button-border-selected";
    }
    return "button-border";
  }

  // Button Order: Black, White, Yellow, Blue, Green, Red
  render() {
    let ellipses;
    console.log("Selected Folder=" + JSON.stringify(this.props.selectedFolder));
    if (this.props.folderButtons) {
      ellipses = this.props.folderButtons.map(button => {
        return (
          <ellipse
            ry="31.24161"
            rx="30.401136"
            cy={button.cy}
            cx={button.cx}
            id={button.name}
            key={button.name}
            onClick={this.props.onFolderButtonClicked}
            className={this.getClassName(button)}
          />
        );
      });
    } else {
      ellipses = [];
    }
    console.log(ellipses);
    return (
      <svg className="App-player" viewBox="0 0 793.59998 427.51999" version="1.1">
        <defs id="defs3717" />
        <image
          y="0"
          x="0"
          id="background"
          xlinkHref={hoerma_sketch}
          preserveAspectRatio="none"
          height="427.51999"
          width="793.59998"
        />
        {ellipses}
      </svg>
    );
  }
}
