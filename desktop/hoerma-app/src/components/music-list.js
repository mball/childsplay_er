import React from "react";
import ReactDropzone from "react-dropzone";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import renderMusicListItem from "./music-list-item.js";
import "../css/App.css";
import { withSnackbar } from "notistack";
import getMessage from "../messages.js";
import MusicListControl from "./music-list-control.js";
const {
  requestSongList,
  updateFolder,
  registerLoadedSongListListener,
  removeLoadedSongListListener
} = window.childsPlayer;

const createOnlyMP3sMsg = getMessage("acceptsOnlyMP3s");
const createFileRemovedMsg = getMessage("fileRemoved");
const createFileAlreadyInList = getMessage("fileAlreadyInList");

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);
  return result;
};

const grid = 4;

const listDefaultBackGround = "lightgrey";

const getListStyle = isDraggingOver => ({
  background: isDraggingOver ? "lightblue" : listDefaultBackGround,
  padding: grid,
  minHeight: "200px"
});

class MusicList extends React.Component {
  constructor(props) {
    super(props);
    this.onDragEnd = this.onDragEnd.bind(this);
    this.onFilesLoaded = this.onFilesLoaded.bind(this);
    this.state = { files: [] };
  }

  onFilesLoaded(files) {
    this.setState({ files });
  }

  songListListener = (event, arg) => {
    this.onFilesLoaded(arg);
  };

  onDrop = (acceptedFiles, rejectedFiles) => {
    const mp3Files = acceptedFiles.filter(element => element.name.split(".").pop() === "mp3");

    if (rejectedFiles.length > 0 && mp3Files.length < acceptedFiles.length) {
      this.props.enqueueSnackbar(createOnlyMP3sMsg(), { variant: "warning" });
    }

    const uniqueFiles = mp3Files.filter(element => {
      //Enforce path only once added as it the unique identifier
      for (let i = 0; i < this.state.files.length; i++) {
        if (this.state.files[i].path === element.path) {
          this.props.enqueueSnackbar(createFileAlreadyInList({ fileName: element.name }), { variant: "warning" });
          return false;
        }
      }
      return true;
    });

    const addedFiles = uniqueFiles.map(acceptedFile => {
      return { name: acceptedFile.name, path: acceptedFile.path };
    });

    this.setState(
      prevState => ({
        files: [...prevState.files, ...addedFiles]
      }),
      () => {
        if (this.props.folderName) {
          updateFolder(this.props.folderName, this.state.files); //Order changed
        }
      }
    );
  };

  onDragEnd(result) {
    const files = result.destination
      ? reorder(this.state.files, result.source.index, result.destination.index)
      : this.deleteFileFromList(result.draggableId);
    if (this.props.folderName) {
      updateFolder(this.props.folderName, files); //Order changed
    }
    this.setState({
      files
    });
  }

  deleteFileFromList = pathToDelete => {
    const file = this.state.files.find(element => element.path === pathToDelete);
    this.props.enqueueSnackbar(createFileRemovedMsg({ fileName: file.name }), { variant: "info" });
    return this.state.files.filter(element => {
      return pathToDelete !== element.path;
    });
  };

  componentDidMount() {
    //Register listener for loaded songlist
    registerLoadedSongListListener(this.songListListener);
    if (this.props.folderName) {
      requestSongList(this.props.folderName);
    }
  }

  componentWillUnmount() {
    removeLoadedSongListListener(this.songListListener);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.folderName !== this.props.folderName) {
      requestSongList(newProps.folderName);
    }
  }

  render() {
    return (
      <div className="App-list">
        <ReactDropzone onDrop={this.onDrop} accept="audio/*">
          {({ getRootProps }) => (
            <div
              {...getRootProps()}
              style={{
                backgroundColor: this.props.folderColor,
                padding: "2.5% 5%"
              }}
            >
              <DragDropContext onDragEnd={this.onDragEnd}>
                <Droppable droppableId="music-list">
                  {(provided, snapshot) => (
                    <div ref={provided.innerRef} style={getListStyle(snapshot.isDraggingOver)}>
                      {this.state.files.map((file, index) => renderMusicListItem(file, index))}
                      {provided.placeholder}
                    </div>
                  )}
                </Droppable>
              </DragDropContext>
              <MusicListControl
                folderName={this.props.folderName}
                style={{
                  backgroundColor: listDefaultBackGround
                }}
              ></MusicListControl>
            </div>
          )}
        </ReactDropzone>
      </div>
    );
  }
}

export default withSnackbar(MusicList);
