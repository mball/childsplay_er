import React from "react";
import getMessage from "../messages";
const { updateFolder, loadFolderFromFile, saveFolderToFile } = window.childsPlayer;

const saveListLabel = getMessage("saveList")();
const loadListLabel = getMessage("loadList")();
const clearListLabel = getMessage("clearList")();

const MusicListControl = props => {
  console.log(JSON.stringify(props));

  const clearList = () => {
    updateFolder(props.folderName, [], true);
  };

  const saveList = () => {
    saveFolderToFile(props.folderName);
  };

  const loadList = () => {
    loadFolderFromFile(props.folderName);
  };

  return (
    <div
      style={{
        ...props.style,
        padding: "1%",
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-evenly"
      }}
    >
      <button onClick={saveList}>{saveListLabel}</button>
      <button onClick={loadList}>{loadListLabel}</button>
      <button onClick={clearList}>{clearListLabel}</button>
    </div>
  );
};

export default MusicListControl;
