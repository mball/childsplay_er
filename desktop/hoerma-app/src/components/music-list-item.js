import React from "react";
import { Draggable } from "react-beautiful-dnd";
import "../css/App.css";

const grid = 4;

const getDraggableItemColor = (isDragging, draggingOver) => {
  return isDragging ? (draggingOver ? "lightgreen" : "lightcoral") : "grey";
};

const getItemStyle = (isDragging, draggingOver, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  ...draggableStyle,
  userSelect: "none",
  padding: `${grid * 1.5}px ${grid * 5}px`,
  margin: `0 0 ${grid}px 0`,
  background: getDraggableItemColor(isDragging, draggingOver)
});

const addDeleteAnimation = (style, snapshot) => {
  if (!snapshot.isDropAnimating || snapshot.draggingOver !== null) {
    return style;
  }
  const { moveTo } = snapshot.dropAnimation;
  // Item was deleted, fade out!
  return {
    ...style,
    transform: `translate(${moveTo.x}px, ${moveTo.y}px)`,
    transition: `ease 1s`,
    opacity: 0
  };
};

const renderDraggable = (provided, snapshot, file) => {
  const style = addDeleteAnimation(
    getItemStyle(snapshot.isDragging, snapshot.draggingOver, provided.draggableProps.style),
    snapshot
  );
  if (snapshot.isDragging) {
    console.log(JSON.stringify(snapshot));
    console.log(JSON.stringify(style));
  }
  return (
    <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps} style={style}>
      {file.name}
    </div>
  );
};

const renderMusicListItem = (file, index) => {
  return (
    <Draggable key={file.path} draggableId={file.path} index={index}>
      {(provided, snapshot) => renderDraggable(provided, snapshot, file)}
    </Draggable>
  );
};

export default renderMusicListItem;
