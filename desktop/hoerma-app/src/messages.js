import template from "lodash.template";

const messages = {
  writeSDCardButton: "Hörma überspielen",
  clickAButtonText: "Drücke einen Knopf um eine Liste auszuwählen",
  saveList: "Speichern",
  loadList: "Laden",
  clearList: "Löschen",
  missingMessage: "Hier fehlt eine Nachricht",
  acceptsOnlyMP3s: "Es können nur MP3s hinzugefügt werden"
};

/* eslint-disable no-template-curly-in-string */
const messagesWithTemplate = {
  fileRemoved: template('Song "${fileName}" entfernt'),
  fileAlreadyInList: template('"${fileName}" ist schon in der Liste!')
};
/* eslint-enable no-template-curly-in-string */

const getMessageMethodByKey = key => (messagesWithTemplate[key] ? messagesWithTemplate[key] : () => messages[key]);

const getMessage = key => getMessageMethodByKey(key);

export default getMessage;
