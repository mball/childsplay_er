# ChildsPlay_er

A MP3 player with a simple interface. This build uses an [DFPlayer Mini](https://www.dfrobot.com/wiki/index.php/DFPlayer_Mini_SKU:DFR0299) as a platform
to build an mp3 player with a simplified interface. Instead of using the standalone
features of this module, this project uses an arduino nano to record button presses and to control the DFPlayer via serial port. Inspired is this build by the music player for kids [Hoerbert](https://www.hoerbert.com/). 
The software is constructed to easily add, remove or reassign buttons to make different casings possible.


![](images/front.jpg) 


TODOs for Docu:
  * Add "install" and/or required library section
  * Explain necessary folder structure
  * Explain buttons
  * Explain pitfalls
  * Change VS Code strucutre

